package com.kage.currency.entity

import org.junit.Test

import org.junit.Assert.*
import timber.log.Timber

class CurrencyTest {

  companion object{
    val EUR_BASE = Currency(code ="EUR",name = "Euro",flagUrl = "",rate = 1f, isBase = true)
    val EUR = Currency(code ="EUR",name = "Euro",flagUrl = "",rate = 0.9f, isBase = false)
    val USD_BASE = Currency(code ="USD",name = "U.S. Dollar",flagUrl = "",rate = 1f, isBase = true)
    val USD = Currency(code ="USD",name = "U.S. Dollar",flagUrl = "",rate = 1.1f, isBase = false)
  }

  @Test
  fun compareTo() {
    assertTrue(EUR_BASE > USD)
    assertTrue(EUR < USD)
    assertTrue(EUR < USD_BASE)
    assertTrue(EUR == EUR)
    assertTrue(EUR_BASE == EUR_BASE)
  }
}