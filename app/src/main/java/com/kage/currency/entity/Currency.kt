package com.kage.currency.entity

data class Currency(
  val code: String,
  val name: String,
  val flagUrl: String,
  val rate: Float,
  val isBase: Boolean
) : Comparable<Currency> {

  /**
   * base currency always first, all others sorted alphabetically by currency code
   */
  override fun compareTo(other: Currency): Int {
    if (this.isBase && other.isBase) return 0
    if (this.isBase) return 1
    return this.code.compareTo(other.code)
  }
}