package com.kage.currency.entity

sealed class Result<out T : Any> {

  /**
   * Base wrapper class for successful data request
   * @return requested data, if data processing was correct
   */
  data class Success<out T : Any>(val data: T) : Result<T>()

  /**
   * Base Wrapper for unsuccessful data request
   */
  data class Error(val exception: Throwable) : Result<Nothing>()

}

/**
 * Exception for 4XX-5XX HTTP errors
 */
class RequestException(message:String):Exception(message)
