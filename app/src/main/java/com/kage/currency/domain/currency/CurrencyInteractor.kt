package com.kage.currency.domain.currency

import com.kage.currency.data.repository.CurrencyRepository
import com.kage.currency.domain.Interactor
import com.kage.currency.entity.Currency
import com.kage.currency.entity.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import timber.log.Timber

interface CurrencyInteractor : Interactor {

  suspend fun getCurrencyRates(baseCurrencySymbol: String): Flow<Result<List<Currency>>>
}

class CurrencyInteractorImpl(
  private val currencyRepo: CurrencyRepository
) : CurrencyInteractor {

  @ExperimentalCoroutinesApi
  override suspend fun getCurrencyRates(baseCurrencySymbol: String) = channelFlow {
    while (!isClosedForSend) {
      send(currencyRepo.getLatestCurrencyRates(baseCurrencySymbol))
      kotlinx.coroutines.delay(1_000)
    }
  }
}