package com.kage.currency

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.kage.currency.di.modules.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App :Application(){

  override fun onCreate() {
    super.onCreate()
    initTimber()
    initKoin()
    initAndroidThreeTen()

  }

  private fun initTimber() {
    if (BuildConfig.DEBUG){
      Timber.plant(Timber.DebugTree())
    } else {
      //prod logging
    }
  }

  private fun initKoin() {
    startKoin {
      androidContext(this@App)
      modules(
        listOf(
          appModule,
          networkModule,
          repositoryModule,
          interactorModule,
          presenterModule
        )
      )
    }
  }

  private fun initAndroidThreeTen() {
    AndroidThreeTen.init(this)
  }

}