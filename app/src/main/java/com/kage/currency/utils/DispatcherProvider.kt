package com.kage.currency.utils

import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

interface DispatcherProvider {
  val bgContext: CoroutineContext
  val uiContext: CoroutineContext
}

class DefaultDispatcherProvider : DispatcherProvider {
  override val bgContext = Dispatchers.IO
  override val uiContext = Dispatchers.Main
}