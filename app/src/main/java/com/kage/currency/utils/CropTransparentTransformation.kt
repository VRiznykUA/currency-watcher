package com.kage.currency.utils

import android.graphics.Bitmap
import com.squareup.picasso.Transformation
import java.util.Arrays

class CropTransparentTransformation : Transformation {

  override fun transform(source: Bitmap): Bitmap {

    val height: Int = source.height
    val width: Int = source.width

    var empty = IntArray(width)
    var buffer = IntArray(width)
    Arrays.fill(empty, 0)

    var top = 0
    var left = 0
    var bottom = height
    var right = width

    for (y in 0 until height) {
      source.getPixels(buffer, 0, width, 0, y, width, 1)
      if (!empty.contentEquals(buffer)) {
        top = y
        break
      }
    }

    for (y in height - 1 downTo top + 1) {
      source.getPixels(buffer, 0, width, 0, y, width, 1)
      if (!empty.contentEquals(buffer)) {
        bottom = y
        break
      }
    }

    val bufferSize = bottom - top + 1
    empty = IntArray(bufferSize)
    buffer = IntArray(bufferSize)
    Arrays.fill(empty, 0)

    for (x in 0 until width) {
      source.getPixels(buffer, 0, 1, x, top + 1, 1, bufferSize)
      if (!empty.contentEquals(buffer)) {
        left = x
        break
      }
    }

    Arrays.fill(empty, 0)
    for (x in width - 1 downTo left + 1) {
      source.getPixels(buffer, 0, 1, x, top + 1, 1, bufferSize)
      if (!empty.contentEquals(buffer)) {
        right = x
        break
      }
    }

    val bitmap = Bitmap.createBitmap(source, left, top, right - left, bottom - top)
    source.recycle()

    return bitmap

  }

  override fun key(): String = "CropTransparentTransformation"
}