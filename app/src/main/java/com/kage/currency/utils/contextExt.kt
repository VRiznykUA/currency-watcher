package com.kage.currency.utils

import android.content.Context
import android.widget.Toast

fun Context.toast(message: String, makeLong: Boolean = false) {
  Toast.makeText(this, message, if (makeLong) Toast.LENGTH_LONG else Toast.LENGTH_SHORT).show()
}


