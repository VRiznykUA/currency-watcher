package com.kage.currency.presentation.currency

import com.kage.currency.entity.Currency
import com.kage.currency.presentation.base.MvpView
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale

interface CurrencyView : MvpView {
  fun updateCurrencyList(currencies: List<CurrencyTuple>)
  fun showError(message: String)
}

data class CurrencyTuple(
  val currency: Currency,
  val input: Float
) {
  fun price() = currency.rate * input

  fun priceFormat() =
    DecimalFormat("#,##0.00", DecimalFormatSymbols(Locale.getDefault())).format(price())
}

