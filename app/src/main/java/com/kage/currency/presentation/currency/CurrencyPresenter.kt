package com.kage.currency.presentation.currency

import androidx.lifecycle.viewModelScope
import com.kage.currency.domain.currency.CurrencyInteractor
import com.kage.currency.entity.Currency
import com.kage.currency.entity.Result
import com.kage.currency.presentation.base.BasePresenter
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class CurrencyPresenter(
  val currencyInteractor: CurrencyInteractor
) : BasePresenter<CurrencyView>() {

  companion object{
    const val DEFAULT_INPUT = 100f
  }

  private var loadingJob: Job? = null
  private var input = DEFAULT_INPUT
  private var currencies:List<Currency> = listOf()

  override fun attachView(mvpView: CurrencyView) {
    super.attachView(mvpView)

    loadCurrencyRates("EUR")
  }

  fun onItemClicked(currency: Currency) {
    //cancel previous endless sequence
    if (loadingJob?.isActive == true) loadingJob?.cancel()

    //start new one with updated base code and reset input
    loadCurrencyRates(currency.code)

  }

  private fun loadCurrencyRates(baseCode: String) {
    loadingJob = viewModelScope.launch {
      currencyInteractor.getCurrencyRates(baseCode)
        .collect { result ->
          this.coroutineContext.ensureActive()
          when (result) {
            is Result.Success ->{
              currencies = result.data
              updateCurrenciesList()
            }
            is Result.Error -> {
              this.coroutineContext.cancel()
              mvpView?.showError(result.exception.message ?: "something goes wrong")
            }
          }
        }
    }
  }

  fun onInputChanged(input: Float) {
    this.input = input
    updateCurrenciesList()

  }

  private fun updateCurrenciesList() {
    val currencyTuples = currencies.map { CurrencyTuple(it, input) }
    mvpView?.updateCurrencyList(currencyTuples)
  }
}