package com.kage.currency.presentation.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

/**
 * Base class that implements the Presenter interface and provides a base implementation for
 * attachView() and detachView(). It also handles keeping a reference to the mvpView that
 * can be accessed from the children classes by calling getMvpView().
 *
 * Inheritance from [ViewModel] allows delegating state management to parent class
 * and also use [viewModelScope]
 */
open class BasePresenter<T : MvpView> :ViewModel(),
  Presenter<T> {

  protected var mvpView: T? = null

  override fun attachView(mvpView: T) {
    this.mvpView = mvpView
  }

  override fun detachView() {
    mvpView = null
  }

  val isViewAttached: Boolean
    get() = mvpView != null

  fun checkViewAttached() {
    if (!isViewAttached) throw MvpViewNotAttachedException()
  }

  class MvpViewNotAttachedException : RuntimeException(
    "Please call Presenter.attachView(MvpView) before requesting data to the Presenter"
  )
}