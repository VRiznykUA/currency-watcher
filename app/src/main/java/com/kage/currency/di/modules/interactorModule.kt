package com.kage.currency.di.modules

import com.kage.currency.domain.currency.CurrencyInteractor
import com.kage.currency.domain.currency.CurrencyInteractorImpl
import org.koin.dsl.module

val interactorModule = module {
  factory<CurrencyInteractor> { CurrencyInteractorImpl(currencyRepo = get()) }
}