package com.kage.currency.di.modules

import com.kage.currency.data.AssetsManager
import com.kage.currency.data.AssetsManagerImpl
import com.kage.currency.utils.DefaultDispatcherProvider
import com.kage.currency.utils.DispatcherProvider
import org.koin.dsl.module

val appModule = module {
  single<DispatcherProvider> { DefaultDispatcherProvider() }
  single<AssetsManager> { AssetsManagerImpl(context = get(), gson = get()) }
}