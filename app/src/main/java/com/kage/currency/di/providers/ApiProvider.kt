package com.kage.currency.di.providers

import com.google.gson.Gson
import com.kage.currency.BuildConfig
import com.kage.currency.data.RevolutApi
import com.kage.currency.di.Provider
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

class ApiProvider(
  private val client: OkHttpClient,
  private val gson: Gson
) : Provider<RevolutApi> {

  override fun provide(): RevolutApi = Retrofit.Builder()
    .baseUrl(BuildConfig.API_BASE_URL)
    .client(client)
    .addConverterFactory(GsonConverterFactory.create(gson))
    .build()
    .create()
}