package com.kage.currency.di.providers

import android.content.Context
import com.localebro.okhttpprofiler.OkHttpProfilerInterceptor
import com.kage.currency.BuildConfig
import com.kage.currency.di.Provider
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

class ApiOkHttpClientProvider(
  private val context:Context
) : Provider<OkHttpClient> {

  companion object {
    const val TIMEOUT_IN_SECONDS = 60L
  }

  override fun provide(): OkHttpClient  {

    val headersInterceptor = Interceptor { chain ->
      val builder = chain.request().newBuilder()
        .addHeader("User-Agent", getUserAgentOrNull(context) ?: "")
        .addHeader("Accept", "application/json")
        .addHeader("Content-type", "application/json")

      chain.proceed(builder.build())
    }

    val builder = OkHttpClient.Builder()
      .connectTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
      .readTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
      .writeTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
      .retryOnConnectionFailure(true)
      .addInterceptor(headersInterceptor)

    if (BuildConfig.DEBUG) {
      builder.addInterceptor(HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
      })
      builder.addInterceptor(OkHttpProfilerInterceptor())
    }

    return builder.build()
  }

  private fun getUserAgentOrNull(context: Context): String? {
    val systemUserAgent = System.getProperty("http.agent")
    val customUserAgent: String
    try {
      customUserAgent = "$systemUserAgent; Currency Watcher Android ${context.packageManager
        .getPackageInfo(context.packageName, 0).versionName}"
    } catch (e: Exception) {
      return systemUserAgent
    }
    return customUserAgent
  }

}
