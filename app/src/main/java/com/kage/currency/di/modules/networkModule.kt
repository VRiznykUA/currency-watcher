package com.kage.currency.di.modules

import com.kage.currency.di.providers.ApiOkHttpClientProvider
import com.kage.currency.di.providers.ApiProvider
import com.kage.currency.di.providers.GsonProvider
import com.kage.currency.di.providers.PicassoProvider
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val OK_HTTP_API = "OK_HTTP_API"
const val OK_HTTP_PICASSO = "OK_HTTP_PICASSO"

val networkModule = module {

  single { GsonProvider().provide() }
  single(named(OK_HTTP_API)) { ApiOkHttpClientProvider(context = get()).provide() }
  single(named(OK_HTTP_PICASSO)) { ApiOkHttpClientProvider(context = get()).provide() }
  single { ApiProvider(client = get(named(OK_HTTP_API)), gson = get()).provide() }
  single { PicassoProvider(context = get(), client = get(named(OK_HTTP_PICASSO))).provide() }

}