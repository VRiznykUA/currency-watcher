package com.kage.currency.di.modules

import com.kage.currency.presentation.currency.CurrencyPresenter
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presenterModule = module {
  viewModel { CurrencyPresenter(currencyInteractor = get()) }
}