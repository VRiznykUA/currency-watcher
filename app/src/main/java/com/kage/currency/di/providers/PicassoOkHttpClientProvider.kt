package com.kage.currency.di.providers

import android.content.Context
import com.localebro.okhttpprofiler.OkHttpProfilerInterceptor
import com.kage.currency.BuildConfig
import com.kage.currency.di.Provider
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

class PicassoOkHttpClientProvider(
  private val context:Context
) : Provider<OkHttpClient> {

  companion object {
    const val TIMEOUT_IN_SECONDS = 60L
    const val CACHE_LIFETIME_IN_SECONDS = 60 * 60 * 24
  }

  override fun provide(): OkHttpClient  = OkHttpClient.Builder()
    .connectTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
    .readTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
    .writeTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
    .retryOnConnectionFailure(true)
    .addInterceptor { chain ->
      chain.proceed(chain.request())
        .newBuilder()
        .header("Cache-Control", "max-age=$CACHE_LIFETIME_IN_SECONDS")
        .build()
    }
    .cache(Cache(context.cacheDir, Int.MAX_VALUE.toLong()))
    .build()


}
