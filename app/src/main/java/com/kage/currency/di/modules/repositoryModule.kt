package com.kage.currency.di.modules

import com.kage.currency.data.repository.CurrencyRepository
import com.kage.currency.data.repository.CurrencyRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
  factory<CurrencyRepository> {
    CurrencyRepositoryImpl(
      api = get(),
      dispatchers = get(),
      assetsManager = get()
    )
  }
}
