package com.kage.currency.di.providers

import com.google.gson.Gson
import com.kage.currency.di.Provider

class GsonProvider : Provider<Gson> {

  override fun provide() = Gson()

}
