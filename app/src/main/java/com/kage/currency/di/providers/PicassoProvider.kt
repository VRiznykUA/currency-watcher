package com.kage.currency.di.providers

import android.content.Context
import com.kage.currency.di.Provider
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient
import timber.log.Timber

class PicassoProvider(
  private val context: Context,
  private val client: OkHttpClient
) : Provider<Picasso> {

  override fun provide(): Picasso = Picasso.Builder(context)
    .downloader(OkHttp3Downloader(client))
    .listener { _, uri, exception ->
      Timber.e("Failed to load $uri with ex: $exception")
    }
    .build()
}

