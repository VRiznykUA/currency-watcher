package com.kage.currency.di

/**
 * Wrapper for components that needed some configuration
 */
interface Provider<T> {

  fun provide(): T

}