package com.kage.currency.data

import android.content.Context
import com.google.gson.Gson
import com.kage.currency.data.entity.CurrencyAssetsMetadata
import com.kage.currency.utils.genericType

interface AssetsManager {

  fun getCurrencyMetadata(): List<CurrencyAssetsMetadata>

}

class AssetsManagerImpl(
  val context: Context,
  val gson: Gson
): AssetsManager {

  private lateinit var currencyMetadata:List<CurrencyAssetsMetadata>

  override fun getCurrencyMetadata(): List<CurrencyAssetsMetadata> {

    if (this::currencyMetadata.isInitialized){
      return  currencyMetadata
    }

    val jsonString = context.assets.open(ASSETS_PATH_FLAGS)
      .bufferedReader()
      .use { it.readText() }

    currencyMetadata = gson.fromJson(jsonString, genericType<List<CurrencyAssetsMetadata>>())

    return currencyMetadata
  }

  companion object {
    private const val ASSETS_PATH_FLAGS = "currency_metadata.json"
  }

}
