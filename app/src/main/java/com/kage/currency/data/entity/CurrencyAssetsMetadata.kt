package com.kage.currency.data.entity

import com.google.gson.annotations.SerializedName

data class CurrencyAssetsMetadata(
  @SerializedName("currency") val currency: String,
  @SerializedName("name") val name: String,
  @SerializedName("flag") val flag: String
)