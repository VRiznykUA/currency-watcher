package com.kage.currency.data

/**
 * mapper from/to  business entity
 */
interface Converter<In, Out> {
  fun convert(input: In): Out
}