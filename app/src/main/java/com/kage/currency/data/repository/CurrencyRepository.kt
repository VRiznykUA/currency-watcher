package com.kage.currency.data.repository

import com.kage.currency.data.AssetsManager
import com.kage.currency.data.Repository
import com.kage.currency.data.RevolutApi
import com.kage.currency.data.converter.CurrencyConverter
import com.kage.currency.entity.Currency
import com.kage.currency.entity.RequestException
import com.kage.currency.entity.Result
import com.kage.currency.utils.DispatcherProvider

interface CurrencyRepository : Repository {
  suspend fun getLatestCurrencyRates(baseCurrencySymbol: String): Result<List<Currency>>
}

class CurrencyRepositoryImpl(
  private val api: RevolutApi,
  private val dispatchers: DispatcherProvider,
  private val assetsManager: AssetsManager
) : CurrencyRepository {

  private val currencyConverter by lazy {
    CurrencyConverter(
      assetsManager.getCurrencyMetadata()
    )
  }

  override suspend fun getLatestCurrencyRates(baseCurrencySymbol: String) =
    with(dispatchers.bgContext) {
      try {
        val response = api.getCurrencyRate(baseCurrencySymbol)

        if (!response.isSuccessful) {
          return@with Result.Error(
            RequestException(
              "${response.code()}:${response.errorBody() ?: ""}"
            )
          )
        }

        val responseData =
          response.body() ?: return@with Result.Error(
            RequestException("empty body")
          )

        Result.Success(currencyConverter.convert(responseData))
      } catch (e: Exception) {
        Result.Error(e)
      }
    }

}