package com.kage.currency.data.entity

import com.google.gson.annotations.SerializedName

data class CurrencyRateResponse(
  @SerializedName("baseCurrency") val baseCurrency: String,
  @SerializedName("rates") val rates: Map<String, Float>

)