package com.kage.currency.data.converter

import com.kage.currency.data.Converter
import com.kage.currency.data.entity.CurrencyAssetsMetadata
import com.kage.currency.data.entity.CurrencyRateResponse
import com.kage.currency.entity.Currency

class CurrencyConverter(
  metadata: List<CurrencyAssetsMetadata>
) : Converter<CurrencyRateResponse, List<Currency>> {

  private val metadataMap = metadata.map { it.currency to it }.toMap()

  override fun convert(input: CurrencyRateResponse): List<Currency> {
    val list = mutableListOf<Currency>()


    metadataMap[input.baseCurrency]?.let {
      list.add(
        Currency(
          code = it.currency,
          name = it.name,
          flagUrl = it.flag,
          rate = 1f,
          isBase = true
        )
      )
    } ?: throw IllegalArgumentException("missing currency ${input.baseCurrency} in metadata")

    input.rates.forEach { entry ->
      metadataMap[entry.key]?.let {
        list.add(
          Currency(
            code = it.currency,
            name = it.name,
            flagUrl = it.flag,
            rate = entry.value,
            isBase = false
          )
        )
      } ?: throw IllegalArgumentException("missing currency ${entry.key} in metadata")
    }

    return list.toList()

  }

}