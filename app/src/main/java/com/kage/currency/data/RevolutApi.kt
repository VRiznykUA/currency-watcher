package com.kage.currency.data

import com.kage.currency.data.entity.CurrencyRateResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutApi {

  @GET("latest")
  suspend fun getCurrencyRate(@Query("base") base: String): Response<CurrencyRateResponse>

}