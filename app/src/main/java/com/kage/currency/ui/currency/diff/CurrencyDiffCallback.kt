package com.kage.currency.ui.currency.diff

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import com.kage.currency.presentation.currency.CurrencyTuple

class CurrencyDiffCallback(
  private val old: List<CurrencyTuple>,
  private val new: List<CurrencyTuple>
) : DiffUtil.Callback() {

  companion object {
    const val PAYLOAD_PRICE = "payload_price"
    const val PAYLOAD_BASE = "payload_base"
  }

  override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
    return old[oldItemPosition].currency.code == new[newItemPosition].currency.code
  }

  override fun getOldListSize() = old.size

  override fun getNewListSize() = new.size

  override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
    return old[oldItemPosition].price() == new[newItemPosition].price()
        && old[oldItemPosition].currency.isBase == new[newItemPosition].currency.isBase
  }

  override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
    return Bundle().apply {
      if (old[oldItemPosition].price() != new[newItemPosition].price()) {
        putFloat(PAYLOAD_PRICE, new[newItemPosition].price())
      }
      if (old[oldItemPosition].currency.isBase != new[newItemPosition].currency.isBase) {
        putBoolean(PAYLOAD_BASE, new[newItemPosition].currency.isBase)
      }
    }
  }

}