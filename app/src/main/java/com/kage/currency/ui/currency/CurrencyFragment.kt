package com.kage.currency.ui.currency

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import com.kage.currency.databinding.FragmentConverterBinding
import com.kage.currency.entity.Currency
import com.kage.currency.presentation.currency.CurrencyPresenter
import com.kage.currency.presentation.currency.CurrencyTuple
import com.kage.currency.presentation.currency.CurrencyView
import com.kage.currency.ui.currency.adapter.CurrencyAdapter
import com.kage.currency.ui.currency.diff.CurrencyDiffCallback
import com.kage.currency.ui.currency.diff.CurrencyUpdateCallback
import com.kage.currency.utils.toast
import com.squareup.picasso.Picasso
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class CurrencyFragment : Fragment(), CurrencyView {

  private lateinit var binding: FragmentConverterBinding
  private lateinit var adapter: CurrencyAdapter
  private val picasso: Picasso by inject()
  private val presenter: CurrencyPresenter by viewModel()

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    binding = FragmentConverterBinding.inflate(inflater, container, false)

    return binding.root
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    presenter.attachView(this)

    adapter = CurrencyAdapter(
      picasso,
      object :
        CurrencyAdapter.OnItemListener {
        override fun onClick(currency: Currency) {
          presenter.onItemClicked(currency)
        }

        override fun onInputChanged(input: Float) {
          presenter.onInputChanged(input)
        }
      })

    binding.rvCurrencies.adapter = adapter
  }

  override fun onDestroy() {
    super.onDestroy()
    presenter.detachView()
  }

  override fun updateCurrencyList(currencies: List<CurrencyTuple>) {
    val diff = DiffUtil.calculateDiff(
      CurrencyDiffCallback(
        adapter.currencies,
        currencies
      )
    )
    diff.dispatchUpdatesTo(
      CurrencyUpdateCallback(
        binding.rvCurrencies,
        adapter
      )
    )
    adapter.currencies = currencies

  }

  override fun showError(message: String) {
    context?.toast(message)
  }
}