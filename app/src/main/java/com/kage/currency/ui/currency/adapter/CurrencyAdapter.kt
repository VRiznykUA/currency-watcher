package com.kage.currency.ui.currency.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kage.currency.databinding.ViewCurrencyItemBinding
import com.kage.currency.entity.Currency
import com.kage.currency.presentation.currency.CurrencyTuple
import com.squareup.picasso.Picasso

class CurrencyAdapter(
  private val picasso: Picasso,
  private val onItemListener: OnItemListener
) : RecyclerView.Adapter<CurrencyVH>() {

  init {
    setHasStableIds(true)
  }

  var currencies: List<CurrencyTuple> = listOf()

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyVH {
    val inflater = LayoutInflater.from(parent.context)
    return CurrencyVH(
      picasso,
      ViewCurrencyItemBinding.inflate(inflater, parent, false),
      onItemListener
    )
  }

  override fun onBindViewHolder(holder: CurrencyVH, position: Int) =
    holder.bind(currencies[position])

  override fun getItemId(position: Int): Long {
    return currencies[position].currency.code.hashCode().toLong()
  }

  override fun onBindViewHolder(holder: CurrencyVH, position: Int, payloads: MutableList<Any>) {
    if (payloads.isNotEmpty()) {
      val bundle = payloads[0] as Bundle
      holder.bind(currencies[position], bundle)
    } else {
      super.onBindViewHolder(holder, position, payloads)
    }
  }

  override fun getItemCount(): Int = currencies.size

  interface OnItemListener {
    fun onClick(currency: Currency)
    fun onInputChanged(input: Float)
  }

}