package com.kage.currency.ui.currency.adapter

import android.os.Bundle
import android.text.TextWatcher
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.RecyclerView
import com.kage.currency.R
import com.kage.currency.databinding.ViewCurrencyItemBinding
import com.kage.currency.presentation.currency.CurrencyTuple
import com.kage.currency.ui.currency.diff.CurrencyDiffCallback
import com.kage.currency.utils.CropTransparentTransformation
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import jp.wasabeef.picasso.transformations.CropSquareTransformation
import timber.log.Timber

class CurrencyVH(
  private val picasso: Picasso,
  private val binding: ViewCurrencyItemBinding,
  private val onItemListener: CurrencyAdapter.OnItemListener
) : RecyclerView.ViewHolder(binding.root) {

  private var textWatcher: TextWatcher? = null

  fun bind(item: CurrencyTuple) = with(binding) {

    tvName.text = item.currency.name
    tvCode.text = item.currency.code

    picasso.load(item.currency.flagUrl)
      .transform(CropTransparentTransformation())
      .transform(CropCircleTransformation())
      .into(ivFlag)

    root.setOnClickListener { onItemListener.onClick(item.currency) }
    etPrice.setOnClickListener { onItemListener.onClick(item.currency) }

    setupPriceView(item, false)
    setupFocus(item.currency.isBase)

  }

  fun bind(item: CurrencyTuple, payload: Bundle) {
    payload.keySet().forEach {
      if (it == CurrencyDiffCallback.PAYLOAD_PRICE) {
        setupPriceView(item, true)
      }
      if (it == CurrencyDiffCallback.PAYLOAD_BASE) {
        setupFocus(item.currency.isBase)
      }
    }
  }

  private fun setupPriceView(item: CurrencyTuple, skipPriceForBase:Boolean) = with(binding){
    //order is matter, need to remove TextWatcher first
    textWatcher?.let { etPrice.removeTextChangedListener(it)}


    if (skipPriceForBase && item.currency.isBase){
      /*NOP*/
    } else{
      setPrice(item)
    }

    if (item.currency.isBase){
      textWatcher = etPrice.addTextChangedListener {
        val input = it?.toString()?.toFloatOrNull() ?: 0f
        Timber.wtf("SPG ${item.currency.code} currentInput=$input")
        onItemListener.onInputChanged(input)
      }
    }
  }

  private fun setPrice(item: CurrencyTuple) {
    if (item.input > 0){
      binding.etPrice.setText(item.priceFormat())
    } else {
      binding.etPrice.text = null
    }
  }

  private fun setupFocus(isBase: Boolean) = with(binding) {
    if (isBase){
      etPrice.isFocusable = true
      etPrice.isFocusableInTouchMode = true
      etPrice.requestFocus()
      etPrice.setSelection(etPrice.text.length)
    } else {
      etPrice.isFocusable = false
      etPrice.isFocusable = false
      etPrice.clearFocus()
    }
  }
}