package com.kage.currency.ui.currency.diff

import androidx.recyclerview.widget.ListUpdateCallback
import androidx.recyclerview.widget.RecyclerView
import com.kage.currency.ui.currency.adapter.CurrencyAdapter

class CurrencyUpdateCallback(
  private val recyclerView: RecyclerView,
  private val adapter: CurrencyAdapter
) : ListUpdateCallback {
  override fun onChanged(position: Int, count: Int, payload: Any?) {
    adapter.notifyItemRangeChanged(position, count, payload)
  }

  override fun onMoved(fromPosition: Int, toPosition: Int) {
    adapter.notifyItemMoved(fromPosition, toPosition)
    recyclerView.scrollToPosition(0)
  }

  override fun onInserted(position: Int, count: Int) {
    adapter.notifyItemRangeInserted(position, count)
  }

  override fun onRemoved(position: Int, count: Int) {
    adapter.notifyItemRangeRemoved(position, count)
  }

}