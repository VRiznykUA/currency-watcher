# Currency Watcher

Test project for Revolut

# Tech stack

    - OkHttp
    - Retrofit
    - Picasso
    - Gson
    - Koin
    - Coroutines
    - ViewModel (Android JetPack)
    - Timber
    
# Architecture

Project uses Clean Architecture principles with 3 main layers: data, domain, presentation
    
## data layer
Main interface - Repository

Repository must receive and produce domain entities, network dto and business entities must be separated
background work handles by suspend functions

Repository must be stateless, if state is needed, it must be resolved by dependency injection

## domain layer
Main interface - Interactor

Domain code must be platform independent for ez unit testing

Interactor can be stateless or stateful. Stateful interactor must implement pattern "Single Instance"

## presentation layer
Uses MVP for UI decomposition

Kinda experimental, I wand to try to use Android JetPack ViewModel as Presenter, 
because of state management, supporting coroutines scope from the box and easy integration with Koin.

# Notes

[Flags](https://www.countryflags.io/)

[Design](https://www.figma.com/file/cUsxw4zNAvU47ADDCJemBM/Rates)



